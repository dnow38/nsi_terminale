def fct_pure(a) :

    a = a + 1
    print("Dans la fonction a = ", a)
    
a = 3
print("Dans le programme principal avant appel de la fonction : a = ", a)
fct_pure(a)
print("Dans le programme principal après appel de la fonction : a = ", a)
