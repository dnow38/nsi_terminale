def fct(lst):
    lst[0] = lst[0] + 1 # On ajoute 1 au premier élément de lst
    print("Dans la fonction lst = ", lst)
    
lst = [1, 2, 3]
print("Dans le programme principal avant appel de la fonction : lst = ", lst)  
fct(lst)
print("Dans le programme principal après appel de la fonction : lst = ", lst)
