def parcours_par_valeur(donnees):
    for val in donnees:
        print(val)  # Quelque chose avec val

def parcours_par_indice(donnees):
    for i in range(len(donnees)):
        print(i, donnees[i])  # Quelque chose avec donnees[i] et/ou i

