# tests
assert derniere_position(7, [5, -1, 7, 4, 6, 4, 2]) == 2
assert derniere_position(4, [5, -1, 7, 4, 6, 4, 2]) == 5
assert derniere_position(0, [5, -1, 7, 4, 6, 4, 2]) == None
# tests secrets
assert derniere_position(0, []) == None
assert derniere_position(0, [0]) == 0
assert derniere_position(0, [0, 0]) == 1
assert derniere_position(0, [1, 0, 2]) == 1
assert derniere_position(0, [0, 2, 1]) == 0
