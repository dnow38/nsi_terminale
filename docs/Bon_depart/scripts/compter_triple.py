def compter_triples(entiers):
    ...

# Tests
assert compter_triples([1, 3, 18, 5]) == 2
assert compter_triples([15, 3, 18]) == 3
assert compter_triples([5, 13, 11, 2]) == 0
assert compter_triples([]) == 0
assert compter_triples([5, 13,0, 2]) == 1

