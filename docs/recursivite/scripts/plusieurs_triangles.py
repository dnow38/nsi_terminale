# --------- PYODIDE:env --------- #
from js import document
if "restart" in globals():
    restart()

def m_a_j(cible):
    done()
    document.getElementById(cible).innerHTML = Screen().html

_cible = 'cible_2'

# --------- PYODIDE:code --------- #
from turtle import *

def motif(cote, n) :
    if n == 1 :
        for i in range(3) :
            forward(cote)
            left(120)
    else : 
        for i in range(3) :
            motif(cote/2, n - 1)
            forward(cote)
            left(120)

#hideturtle()  # on cache la tortue
#speed(0) # tortue rapide
cote = 200

motif(cote, 1)

# --------- PYODIDE:post --------- #
if Screen().html is None:
    forward(0)
m_a_j(_cible)
# --------- PYODIDE:post_term --------- #
if "m_a_j" in globals():
    m_a_j(_cible)