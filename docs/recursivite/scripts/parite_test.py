# Tests
assert est_pair(0) is True
assert est_pair(1) is False
assert est_pair(12) is True
assert est_pair(13) is False

# Autres tests

assert est_pair(38) is True
assert est_pair(17) is False
assert est_pair(16) is True
assert est_pair(27) is False