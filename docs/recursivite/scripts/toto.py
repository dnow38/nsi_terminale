def toto(n):
    ...


# Tests

assert toto(0) == '0'
assert toto(1) == '(0 + 0)'
assert toto(2) == '((0 + 0) + (0 + 0))'

