def somme(n):
    ...


assert somme(0) == 0
assert somme(1) == 1
assert somme(3) == 6
assert somme(5) == 15
