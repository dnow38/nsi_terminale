def nbre_chemins(n):
    if n == 1:
        return 1
    elif n == 2:
        return 2
    else:
        return nbre_chemins(n - 1) + nbre_chemins(n - 2)

