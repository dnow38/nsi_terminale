def recherche(lst, x):
    """
    Cette fonction renvoie True si x est dans lst, et False sinon
    Précondition : x : int, lst : list et triée en ordre croissant
    Postcondition : cette fonction renvoie du type booléen
    """

    if len(lst) == 0:
        return False
    m = len(lst) // 2
    if lst[m] == x:
        return True
    elif lst[m] < x:
        return recherche(lst[m + 1:], x)
    else:
        return recherche(lst[:m], x)


