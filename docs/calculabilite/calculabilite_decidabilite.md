---
authors: Gilles Lassus et Mireille Coilhac
title: 9) Calculabilité et décidabilité
---

Vous trouverez un cours ici : 
[Calculabilité et décidabilité par Gilles Lassus](https://glassus.github.io/terminale_nsi/T2_Programmation/2.3_Calculabilite_Decidabilite/cours/){ .md-button target="_blank" rel="noopener" }


## Crédits 

Gilles Lassus