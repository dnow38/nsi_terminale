def parcours_suffixe_liste(arbre):
    if arbre == None : 
        return []
    return parcours_suffixe_liste(arbre.gauche) + parcours_suffixe_liste(arbre.droit) + [arbre.valeur]
