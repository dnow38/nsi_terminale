---
author:  Richardot C./Nowakowski D.
title: Accueil
---

## Terminale NSI lycée Camille Corot

Voici un nouvel espace dans lequel vous trouverez des ressources concernant les cours de NSI en terminale :


- un cours complet avec des exemples  


- des exercices corrigés, que ce soit en programmation ou autre  


- des QCM pour préparer vos évaluations  
    

- bientot : des ressources pour préparer le bac ainsi que le grand oral  

!!! info "Attention "

	Cet espace vient en complément des cours que nous faisons ensemble !

Je termine avec une petite citation :  
“N’importe quel idiot peut écrire du code qu'un ordinateur peut comprendre. Les bons programmeurs écrivent du code que les humains peuvent comprendre.”  
 *Martin Fowler*


### Site de référence

Je remercie chalheureusement les enseignants partageant leur ressources.
Les différentes pages du site ont pour origines : 


- [https://glassus.github.io/](https://glassus.github.io/)


- [https://mcoilhac.forge.apps.education.fr/site-nsi/](https://mcoilhac.forge.apps.education.fr/site-nsi/)


- [https://nreveret.forge.apps.education.fr/donnees_en_table/](https://nreveret.forge.apps.education.fr/donnees_en_table/)
    

