---
author: Mireille Coilhac
title: Exercices
---

## Plus longues sous-suites communes

[Plus longues sous-suites communes](https://codex.forge.apps.education.fr/exercices/lplssc/){ .md-button target="_blank" rel="noopener" }

## Somme maximale de k termes consécutifs

[Somme maximale de k termes consécutifs](https://codex.forge.apps.education.fr/exercices/k_consecutifs_1/){ .md-button target="_blank" rel="noopener" }


## Somme maximale dans une grille

[Somme maximale dans une grille](https://codex.forge.apps.education.fr/en_travaux/somme_maximale/){ .md-button target="_blank" rel="noopener" }

## Communication des acacias

[Communication des acacias](https://codex.forge.apps.education.fr/exercices/acacias/){ .md-button target="_blank" rel="noopener" }

## Nombre de chemins dans une grille

[Nombre de chemins dans une grille](https://codex.forge.apps.education.fr/exercices/nb_chemins_grille/){ .md-button target="_blank" rel="noopener" }

## Produit maximal de k termes consécutifs (attendre la future mise à jour de cet exercice)

[Produit maximal de k termes consécutifs](https://codex.forge.apps.education.fr/exercices/k_consecutifs_2/){ .md-button target="_blank" rel="noopener" }
