from math import sqrt

class TriangleRect:
    def __init__(self, cote1, cote2):
        self.cote1 = cote1
        self.cote2 = cote2
        self.hypotenuse = sqrt(cote1**2 + cote2**2)

