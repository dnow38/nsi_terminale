class Personnage_6:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_vies(...):
        ...

    def boire_potion(self):
        self.vie = self.vie + 1

gollum = ...
gollum.perd_vies(2)  # gollum perd deux vies
print(...)



