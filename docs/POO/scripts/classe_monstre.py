class Monstre:
    def __init__(self, p, deg) :
        self.pv = p
        self.degats = deg
    
    def recoitDegats(self, deg):
        self.pv = self.pv - deg
        
    def seClonerV1(self):
        return Monstre(self.pv, self.degats)
    
    def seClonerV2(self):
        return self

# mic et son clone version 1
mic = Monstre(30, 10)
mic.recoitDegats(5)
clone1 = mic.seClonerV1()
mic.recoitDegats(3)
clone1.recoitDegats(25)
print("points de vies de mic : ", mic.pv)
print("points de vies du clone v1 de mic : ", clone1.pv)

# moc et son clone version 2
moc = Monstre(30, 15)
moc.recoitDegats(5)
clone2 = moc.seClonerV2()
moc.recoitDegats(3)
clone2.recoitDegats(15)
print("points de vies de moc : ", moc.pv)
print("points de vies du clone v2 de moc : ", clone2.pv)
