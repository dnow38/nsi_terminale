def fausse_piece(tresor, debut, fin):
    """
    Renvoie le numéro de la fausse pièce dans le trésor
    Le trésor initial compte 128 pièces
    """
    if debut + 1 == fin:  # une seule pièce dans la zone d'étude
        return debut

    milieu = (debut + fin) // 2
    observation = compare(tresor, debut, milieu, milieu, fin)
    if observation == "gauche":
        return fausse_piece(tresor, debut, milieu)
    else:
        return fausse_piece(tresor, milieu, fin)
        