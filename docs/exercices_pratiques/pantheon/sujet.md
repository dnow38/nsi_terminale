---
author: Gilles Lassus puis Mireille Coilhac
title: Panthéon
tags:
  - tuple
  - liste/tableau
  - Difficulté **
  - important
---


On considère la fonction `pantheon` prenant en paramètres `eleves` et `notes` deux
tableaux de même longueur, le premier contenant le nom des élèves et le second, des
entiers positifs désignant leur note à un contrôle de sorte que `eleves[i]` a obtenu la
note `notes[i]`.  
Cette fonction renvoie le couple constitué de la note maximale attribuée et des noms
des élèves ayant obtenu cette note regroupés dans un tableau.  

!!! example "Exemple"

    ```pycon
    >>> pantheon(['a', 'b', 'c', 'd'], [15, 18, 12, 18])
    (18, ['b', 'd'])
    ```

???+ question "Compléter la fonction `pantheon`" 

    {{ IDE('exo') }}

    ??? success "Solution"

    Pour bien comprendre le fonctionnement de cet algorithme, avec de nouvelles affectations de la liste `meilleurs_eleves`, à chaque fois que l'on trouve une note meilleure, vous pouvez tester ci-dessous

    {{ IDE('exo_corr_rem') }}


