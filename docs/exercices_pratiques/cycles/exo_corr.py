def est_cyclique(plan):
    """
    Prend en paramètre un dictionnaire `plan` correspondant à un plan d'envoi valide de messages.
    Renvoie True si le plan d'envoi de messages est cyclique et False sinon.
    """
    personnes = [key for key in plan]
    expediteur = personnes[0]
    destinataire = plan[expediteur]
    nb_destinaires = 1

    while destinataire != expediteur:
        destinataire = plan[destinataire]
        nb_destinaires = nb_destinaires + 1

    return nb_destinaires == len(plan)

