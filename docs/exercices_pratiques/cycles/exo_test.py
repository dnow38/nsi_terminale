# Tests
plan_a = {'Anne': 'Elodie', 'Bruno': 'Fidel', 'Claude': 'Denis',
          'Denis': 'Claude', 'Elodie': 'Bruno', 'Fidel': 'Anne'}
plan_b = {'Anne': 'Claude', 'Bruno': 'Fidel', 'Claude': 'Elodie',
          'Denis': 'Anne', 'Elodie': 'Bruno', 'Fidel': 'Denis'}
assert not est_cyclique(plan_a)
assert est_cyclique(plan_b)


# Autres tests
assert est_cyclique({'A': 'B', 'F': 'C', 'C': 'D', 'E': 'A', 'B': 'F', 'D': 'E'})
assert not est_cyclique({'A': 'B', 'F': 'A', 'C': 'D', 'E': 'C', 'B': 'F', 'D': 'E'})
assert est_cyclique({'A': 'E', 'F': 'C', 'C': 'D', 'E': 'B', 'B': 'F', 'D': 'A'})
assert not est_cyclique({'A': 'A', 'B': 'B'})
assert est_cyclique({'A': 'B', 'B': 'A'})
assert est_cyclique({'A': 'B', 'B': 'C', 'C': 'D', 'D': 'A'})
assert not est_cyclique({'A': 'B', 'B': 'C', 'C': 'D', 'D': 'A', 'E': 'E'})