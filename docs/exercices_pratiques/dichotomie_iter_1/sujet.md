---
author: BNS 2022 n° 21.2 puis l'équipe e-nsi
title: Recherche dichotomique itérative (1)
tags:
  - 5-dichotomie
---


# Dichotomie

Compléter la fonction `dichotomie` :

* prenant en paramètre un tableau de nombres triés dans l'ordre croissant `nombres` et une valeur `cible`
  
* renvoyant `True` si `cible` est une valeur de `nombres`, `False` dans le cas contraire.

!!! example "Exemples"

    ```pycon
    >>> dichotomie([1, 2, 3, 4], 2)
    True
    >>> dichotomie([1, 2, 3, 4], 1)
    True
    >>> dichotomie([1, 2, 3, 4], 4)
    True
    >>> dichotomie([1, 2, 3, 4], 5)
    False
    >>> dichotomie([1, 2, 3, 4], 0)
    False
    >>> dichotomie([1], 1)
    True
    >>> dichotomie([1], 0)
    False
    >>> dichotomie([], 1)
    False

    ```

!!! warning "Remarque"

    Vous utiliserez obligatoirement un algorithme de **recherche dichotomique**.
    

???+ question "Compléter ci-dessous"

    {{ IDE('exo') }}
