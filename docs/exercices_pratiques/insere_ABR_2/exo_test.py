# Tests

arbre = Noeud(7)
for cle in (3, 9, 1, 6):
    arbre.inserer(cle)
assert arbre.gauche.etiquette == 3
assert arbre.droit.etiquette == 9
assert arbre.gauche.gauche.etiquette == 1
assert arbre.gauche.droit.etiquette == 6

# Autres tests

arbre = Noeud(2)
for cle in (3, 9, 1, 6):
    arbre.inserer(cle)
assert arbre.gauche.etiquette == 1
assert arbre.droit.etiquette == 3
assert arbre.droit.droit.etiquette == 9
assert arbre.droit.droit.gauche.etiquette == 6
