---
author: Gilles Lassus puis Mireille Coilhac
title: Adresses IP
tags:
  - POO
  - important
---


On définit une classe gérant une adresse IPv4.

On rappelle qu’une adresse IPv4 est une adresse de longueur 4 octets, notée en décimale
à point, en séparant chacun des octets par un point. On considère un réseau privé avec
une plage d’adresses IP de `192.168.0.0` à `192.168.0.255`.

On considère que les adresses IP saisies sont valides.

Les adresses IP `192.168.0.0` et `192.168.0.255` sont des adresses réservées.

Le code ci-dessous implémente la classe `AdresseIP`.



Compléter le code ci-dessous et instancier trois objets : adresse1, adresse2, adresse3 avec respectivement les arguments suivants

'192.168.0.1', '192.168.0.2', '192.168.0.0'

!!! Example "Exemples"

    ```pycon
    >>> adresse1.liste_octets()
    [192, 168, 0, 1]
    >>> adresse1.est_reservee()
    False
    >>> adresse3.est_reservee()
    True
    >>> adresse2.adresse_suivante().adresse # acces valide à adresse
    # ici car on sait que l'adresse suivante existe
    '192.168.0.3'
    ```


???+ question "Compléter le code ci-dessous"

    {{ IDE('exo', MAX_SIZE=45) }}