def nbre_occurrences(cible, valeurs):
    ...


# Tests
assert nbre_occurrences(5, [2, 5, 3, 5, 6, 9, 5]) == 3
assert nbre_occurrences("A", ["B", "A", "B", "A", "R"]) == 2
assert nbre_occurrences(12, [1, 7, 21, 36, 44]) == 0
assert nbre_occurrences(12, []) == 0
