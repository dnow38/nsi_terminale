def partition(pivot, tableau):
    indices_inferieur = []
    indices_egal = []
    indices_superieur = []
    for i in range(len(tableau)):
        if tableau[i] < pivot:
            indices_inferieur.append(i)
        elif tableau[i] > pivot:
            indices_superieur.append(i)
        else:
            indices_egal.append(i)
    return (indices_inferieur, indices_egal, indices_superieur)
