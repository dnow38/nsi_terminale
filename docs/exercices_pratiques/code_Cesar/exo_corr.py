def cesar(message, decalage):
    resultat = ''
    for caractere in message:
        if 'A' <= caractere and caractere <= 'Z':
            indice = (position_alphabet(caractere) + decalage) % 26
            resultat = resultat + ALPHABET[indice]
        else:
            resultat = resultat + caractere
    return resultat
