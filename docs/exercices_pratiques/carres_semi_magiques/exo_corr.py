from math import sqrt

class Carre:
    def __init__(self, nombres):
        self.ordre = int(sqrt(len(nombres)))
        self.tableau = [[nombres[j + i*self.ordre] for j in range(self.ordre)] for i in range(self.ordre)]

    def affiche(self):
        '''Affiche un carré'''
        for ligne in self.tableau:
            print(ligne)

    def somme_ligne(self, i):
        '''Calcule la somme des valeurs de la ligne i'''
        somme = 0
        for j in range(self.ordre):
            somme = somme + self.tableau[i][j]
        return somme

    def somme_colonne(self, j):
        '''Calcule la somme des valeurs de la colonne j'''
        somme = 0
        for i in range(self.ordre):
            somme = somme + self.tableau[i][j]
        return somme

    def est_semi_magique(self):
        somme_commune = self.somme_ligne(0)
        for i in range(self.ordre):
            if self.somme_ligne(i) != somme_commune:
                return False
        for j in range(self.ordre):
            if self.somme_colonne(j) != somme_commune:
                return False              
        return True

