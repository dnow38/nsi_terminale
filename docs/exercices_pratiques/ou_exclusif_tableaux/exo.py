def ou_exclusif_listes(liste_1, liste_2):
    ...

# Tests
liste_a = [1, 0, 1, 0, 1, 1, 0, 1]
liste_b = [0, 1, 1, 1, 0, 1, 0, 0]
liste_c = [1, 1, 0, 1]
liste_d = [0, 0, 1, 1]
assert ou_exclusif_listes(liste_a, liste_b) == [1, 1, 0, 1, 1, 0, 0, 1]
assert ou_exclusif_listes(liste_c, liste_d) == [1, 1, 1, 0]
    
