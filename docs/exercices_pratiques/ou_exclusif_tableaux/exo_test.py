# Tests
liste_a = [1, 0, 1, 0, 1, 1, 0, 1]
liste_b = [0, 1, 1, 1, 0, 1, 0, 0]
liste_c = [1, 1, 0, 1]
liste_d = [0, 0, 1, 1]
assert ou_exclusif_listes(liste_a, liste_b) == [1, 1, 0, 1, 1, 0, 0, 1]
assert ou_exclusif_listes(liste_c, liste_d) == [1, 1, 1, 0]

# Autres tests
from random import randint
liste_e = [randint(0, 1) for i in range(100)]
liste_f = [randint(0, 1) for i in range(100)]
assert ou_exclusif_listes(liste_e, liste_f) == [(liste_e[i] + liste_f[i])%2 for i in range(100)]
