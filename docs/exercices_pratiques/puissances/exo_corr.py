def liste_puissances(a, n):
    puissances = [a]
    for i in range(n-1):
        puissances.append(puissances[-1] * a)
    return puissances

def liste_puissances_borne(a, borne):
    puissances = []
    valeur = a
    while valeur < borne:
        puissances.append(valeur)
        valeur = valeur * a
    return puissances

