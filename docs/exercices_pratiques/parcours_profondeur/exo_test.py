# Tests

assert accessibles([[1, 2], [0], [0, 3], [1], [5], [4]], 0) == [0, 1, 2, 3]  # orienté erreur sujet
assert accessibles([[1, 2], [0], [0, 3], [1], [5], [4]], 4) == [4, 5]  # orienté erreeur du sujet

# Autres tests

assert accessibles([[1, 2], [2], [0], [0]], 0) == [0, 1, 2]
assert accessibles([[1, 2], [0, 3], [0,], [1], [5], [4]], 0) == [0, 1, 3, 2]  # non orienté
assert accessibles([[1, 2], [0, 3], [0,], [1], [5], [4]], 4) == [4, 5]  # non orienté

