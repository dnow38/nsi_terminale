def binaire(a):
    ...

# Tests

assert binaire(0) == '0'
assert binaire(1) == '1'
assert binaire(16) == '10000'
assert binaire(77) == '1001101'


