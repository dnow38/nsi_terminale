VALEURS = [
    (1000, "M"),
    (900, "CM"),
    (500, "D"),
    (400, "CD"),
    (100, "C"),
    (90, "XC"),
    (50, "L"),
    (40, "XL"),
    (10, "X"),
    (9, "IX"),
    (5, "V"),
    (4, "IV"),
    (1, "I"),
]

# Tests
assert romain(4) == "IV"
assert romain(5) == "V"
assert romain(6) == "VI"
assert romain(5042) == "MMMMMXLII"

# Tests supplémentaires
def romain_corr(valeur):
    resultat = ""
    for entier, symbole in VALEURS:
        while valeur >= entier:
            valeur -= entier
            resultat += symbole
    return resultat

for valeur in range(1, 5_001):
    attendu = romain_corr(valeur)
    assert romain(valeur) == attendu, f"Erreur en écrivant {valeur}"
