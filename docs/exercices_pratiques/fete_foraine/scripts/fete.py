#--- HDR ---#
from random import randrange

class Lots_en_jeu():
    """
    Classe mettant en oeuvre un trésor de pièces d'or
    contenant une fausse pièce
    """

    def __init__(self):
        self.taille = 32
        self.n_gagnant = randrange(1, self.taille + 1)

        self.valeur = randrange(1, 50)
        self.gagnant = 100*self.valeur

    def __str__(self):
        """Renvoie la description des lots"""
        if self.taille > 1:
            return f"un jeu de {self.taille} lots"


    def __repr__(self):
        """Renvoie la description des lots"""
        return self.__str__()


def indication(lots, debut_1, fin_1, debut_2, fin_2):
    """compare les valeurs totales des groupes groupe_1 et groupe_2.

    groupe_1 contient les lots dont les numéros de début et de fin sont
    `debut_1` (inclus) et `fin_1` (exclu).
    groupe_2 contient les lots dont les numéros de début et de fin sont
    `debut_2` (inclus) et `fin_2` (exclu).

    Cette fonction renvoie :
    * "groupe_1" si le groupe_1 a la plus grande valeur
    * "identique" si les deux groupes ont la même valeur
    * "groupe_2" si le groupe_2 a la plus grande valeur

    indication(lots, 1, 20, 28, 31) compare donc les valeurs
    des lots de numéros :
    * allant de 1 (inclus) à 20 exclu pour le groupe_1
    * allant de 28 (inclus) à 31 exclu pour le groupe_2
    """

    if debut_1 <= lots.n_gagnant < fin_1:
        valeur_1 = (
            lots.valeur * (fin_1 - debut_1 - 1) + lots.gagnant
        )
    else:
        valeur_1 = lots.valeur * (fin_1 - debut_1)

    if debut_2 <= lots.n_gagnant < fin_2:
        valeur_2 = (
            lots.valeur * (fin_2 - debut_2 - 1) + lots.gagnant
        )
    else:
        valeur_2 = lots.valeur * (fin_2 - debut_2)

    if valeur_1 < valeur_2:
        return "groupe_2"
    elif valeur_1 == valeur_2:
        return "identique"
    else:
        return "groupe_1"


#--- HDR ---#


def gros_lot(lots, debut, fin):
    """
    Renvoie le numéro du gros lot dans lots
    """
    ...
    

# Tests
for i in range(10):
    lots = Lots_en_jeu()
    assert gros_lot(lots, 1, 33) == lots.n_gagnant

    