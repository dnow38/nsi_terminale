def gros_lot(lots, debut, fin):
    """
    Renvoie le numéro du gros lot dans lots
    """
    if debut + 1 == fin:  # une seule pièce dans la zone d'étude
        return debut

    milieu = (debut + fin) // 2
    observation = indication(lots, debut, milieu, milieu, fin)
    if observation == "groupe_1":
        return gros_lot(lots, debut, milieu)
    else:
        return gros_lot(lots, milieu, fin)

