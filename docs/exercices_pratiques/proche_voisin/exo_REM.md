!!! danger "Attention"

	⚠️🌵 **Il faut absolument éviter de comparer deux flottants entre eux.**

	!!! example "Exemple"

		```pycon
		>>> 0.1 + 0.2 > 0.3
		True
		>>> 0.1 + 0.2
		0.30000000000000004
		```

	Dans cet exercice les coordonnées des points sont supposées entières, mais le calcul de distance avec la racine carrée donne des flottants, qui vont être comparés à d'autres nombres.

	Pour travailler avec des entiers, il aurait fallu comparer les carrés des distances, avec évidemment des points de coordonnées entières. Les distances étant des nombres positifs, le problème aurait pû être résolu ainsi.
