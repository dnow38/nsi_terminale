def naif_envers(texte, motif):
    indices = []
    i = len(motif) - 1
    while i <= len(texte) - 1:
        k = 0
        while k < len(motif) and motif[len(motif) - 1 - k] == texte[i - k]:
            k = k + 1
        if k == len(motif):
            indices.append(i - len(motif) + 1)
        i = i + 1
    return indices
    