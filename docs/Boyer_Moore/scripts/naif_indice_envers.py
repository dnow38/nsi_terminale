def naif_envers(texte, motif):
    indices = []
    i = len(motif) - 1
    while i <= len(texte) - 1:
        k = 0
        while k < ... and motif[...] == texte[i - k]:
            k = ...
        if k == ...:
            indices.append(...)
        i = i + 1
    return indices

# Tests
assert naif_envers("une magnifique maison bleue", "maison") == [15]
assert naif_envers("une magnifique maison bleue", "nsi") == []
assert naif_envers("une magnifique maison bleue", "ma") == [4, 15]
