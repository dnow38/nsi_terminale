---
author: Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title:  Recherche naïve
---


## II. Recherche textuelle naïve

Regarder les **5 premières minutes** de la vidéo de l'introduction.

<p>L'algorithme naïf et l'algorithme de Horspool en vidéo : <a target="wims_external" href="https://grafikart.fr/tutoriels/boyer-moore-horspool-1197">Recherche textuelle</a>
</p>

???+ tip "Illustration de l'algorithme"

	Auteur : Gilles Lassus

    ![gif naif](images/gif_naive.gif){ width=90% }



!!! info "Animation de Nicolas Revéret"

    [Recherche naïve](https://boyer-moore.codekodo.net/recherche_naive.php){ .md-button target="_blank" rel="noopener" }



???+ question "Algorithme de recherche naïve"

    Compléter ci-dessous :

    {{ IDE('scripts/naif_indice') }}


???+ question "Version booléenne de l'algorithme de recherche naïve"

    Re-écrire l'algorithme précédent en s'arrêtant dès qu'une occurrence de motif est trouvée dans texte.

    La fonction renverra uniquement un booléen.

    {{ IDE('scripts/naif_bool') }}



!!! info "Temps de recherches"

    ⌛ **Avant de commencer**

    Vous devez travailler sur [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    Télécharger dans **le même dossier** :  

    * 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

    * 🌐 TD `temps_naif.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/temps_naif.ipynb)

    Dans Basthon ouvrir `temps_naif.ipynb`, puis dans `temps_naif.ipynb` ouvrir `pg798.txt` (cliquer sur OK).

