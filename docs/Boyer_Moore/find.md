---
author: Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title:  Méthode `find` de python
---

![illus](images/illus.png){ width=30%; : .center }
> Source : Gilles Lassus


## I. La méthode `find` de Python

!!! info "⌛ Avant de commencer"

    Vous devez travailler sur [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    Télécharger dans **le même dossier** :  

    * 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

    * 🌐 TD `find_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/find_sujet.ipynb)

    Dans Basthon ouvrir `find_sujet.ipynb`, puis dans `find_sujet.ipynb` ouvrir `pg798.txt` (cliquer sur OK).

    😀 La correction est arrivée  ... 

    Télécharger **dans le même dossier** :

    * 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

    * 🌐 Correction du TD `find_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/find_corr.ipynb)

    Dans Basthon ouvrir `find_corr.ipynb`, puis dans `find_corr.ipynb` ouvrir `pg798.txt` (cliquer sur OK).



<!--- 

⏳ La correction viendra bientôt ... 
Télécharger dans le même dossier :

* 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

* 🌐 Correction du TD `find_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/find_corr.ipynb)
-->
