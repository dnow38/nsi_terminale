trinome = lambda a, b, c: lambda x: ...

# Tests
f = trinome(1, 1, 1)
assert f(2) == 7
assert f(0) == 1
assert trinome(3, -1, 2)(6) == 104
