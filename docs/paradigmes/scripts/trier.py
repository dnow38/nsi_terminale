def clef_note(mon_tuple):
      return (mon_tuple[0])

def clef_nom(mon_tuple):
      return (mon_tuple[1])

tab = [(16, 'MARIE') , (16, 'ISMAEL'), (12, 'ANNE'), (17, 'SARAH')]   
print("Par ordre décroissant des notes : ", sorted(tab, key=clef_note, reverse=True))
print("Par ordre alphabétique des noms : ", sorted(tab, key=clef_nom, reverse=False))
