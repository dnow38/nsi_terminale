nombres = (1, 2, 3, 4, 5)
resultat = filter(lambda x: x % 2 == 0, nombres)

print(resultat)  # Un objet itérable
print(type(resultat))
print(list(resultat))
