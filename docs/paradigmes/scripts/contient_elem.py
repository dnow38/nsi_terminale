def contient(liste, v):
    ...


# Tests

assert contient(cons(4, cons(13, cons(1, cons(5, creer_liste())))), 8) is False
assert contient(cons(4, cons(13, cons(1, cons(5, creer_liste())))), 13) is True
assert contient(creer_liste(), 13) is False

