class Cellule :
    def __init__(self, contenu, suivante):
        self.contenu = contenu
        self.suivante = suivante

class Pile:
    def __init__(self):
        self.data = None

    def est_vide(self):
        return self.data == None

    def empile(self, x):
        self.data = Cellule(x, self.data)

    def depile(self):
        v = self.data.contenu  # on récupère la valeur à renvoyer
        self.data = self.data.suivante  # on supprime la 1ère cellule
        return v

    def __str__(self):
        """ Le sommet de la pile est à gauche"""
        s = "sommet -> "
        c = self.data
        while c != None :
            s += str(c.contenu)+"|"
            c = c.suivante
        return s

# Tests
p = Pile()
print("On empile")
for i in range(4):
    p.empile(i)
    print(p)
    
print("On dépile")
for i in range(4):
    p.depile()
    print(p)

# A vous ci-dessous ...

