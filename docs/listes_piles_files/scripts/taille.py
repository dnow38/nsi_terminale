def taille(pile):
    ...

pile_1 = Pile()
pile_1.empiler("a")
pile_1.empiler("b")
print("Au début", pile_1)
print("Taille de la pile : ", taille(pile_1))
print("A la fin", pile_1)

# Tests
assert taille(pile_1) == 2


